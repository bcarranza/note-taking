# Responding to Customers

## Principles

## When I am not sure...

A colleague kindly reminded me that more important than what we as Support Engineers **know** is what we can **figure out**. A bit of advice I find helpful and often share:

> If you encounter something you don't know, you can do two things: learn about it or don't. 

The latter choice is sometimes the right one but when it isn't: the only way out is through. It would not be feasible (for me) to _only_ take tickets where I knew everything in advance. That would also be **extremely** boring.

We are often tasked with coming up to speed on a topic we don't know much about _quickly_. Here are some loosely formed thoughts to the prompt:

> Brie, do you encounter moments when you aren't confident in your answer? If so, how do you approach presenting that to a customer?

**Yes**, I absolutely encounter those moments. In those situations:

  - Identify what you do know and what you don't know
  - I think it's important to be forthright and specific about what I don't know. (Something like "It is not yet clear how the configuration of `foo` will impact the undesired behavior you are encountering with `bar`.")
  - When you tell a customer what you don't know: tell them (BRIEFLY) the plan for figuring it out: I will consult with some colleagues for a better understanding about `foo` and the impact it has on `bar`. The plan for figuring it out should include a note about when the customer can expect an update. I typically tell them to expect "an update on my investigation" rather than "an answer" in case the situation is more complex than it seems at first. 
  - Put myself in the customer's shoes: in my role prior to GitLab, I helped to maintain infrastructure so I was working with vendor support teams. While I do want an answer quickly, I will **always** prefer an accurate answer to a quick one, given that choice. I have yet to find a customer who would prefer an "OK answer now" to a "good answer later today or by EOD tomorrow".      
  - In some situations, we need to try the "OK answer now" _while_ working towards the "good answer later". In those kinds of situations, I make it clear to the customer with language like:
    - "strictly as a troubleshooting measure" - When I want to ask them to try something that could be _perceived_ as me trying to implement a suboptimal workaround. 
    - "I have medium confidence that doing X will lead to the desired outcome". Using confidence levels lets the customer decide whether they want to try something that I propose. They will know more about their organization's decision-making process. For example: some organizations have to run any change to `gitlab.rb` through a change advisory board and schedule a maintenance window meaning a simple "please set this and run `gitlab-ctl reconfigure` and see if that fixes it" could require a week to test. I only want the customer to incur this time-cost if they truly feel it's worthwhile. My confidence level helps them to make that decision. 


We have a **LOT** of amazing resources. The resources I tend to hit first:

  - searching through ZenDesk -- an absolute treasure trove! I _try_ to include detailed internal notes because I see how helpful they are for me. 
  - issue trackers -- there are often **excellent** ideas and tips for troubleshooting in the comments of issues. Just because an issue isn't about the precise behavior you are researching does not mean that the issue is useless! 

The above is in addition to Slack, GitLab Forum and `$SEARCH_ENGINE_OF_CHOICE`. 
  
