# Using these scripts

These scripts are intended to help form part of my response to a customer. I take care to customize the text to suit the specific situation but these to save lots of typing. 

You should modify them to replace **Brie** with your name with a command like `sed -i 's/Brie/Plop/g'`. 



## answer-solve

I use this when I haven't heard from the customer after a few replies. I want to let them know that I'll be closing the ticket when I write my next note. This often motivates people to respond. I want to make it clear that I really do want to hear back from them. Even if I do close the ticket and mark it as **Solved**, they should reply if they would like to continue the conversation.

```
# answer-solve Tuesday

I look forward to your reply. I will plan to check back in with you on **Tuesday**.

If I don't hear from you by **Tuesday**, I will send one last note as I close this ticket and mark it as **Solved**. You are welcome to reply to this message after that time. Your reply will either re-open this ticket or open a new follow-up ticket, depending on how much time has passed.



Thanks!

--
Brie
```

 

## checkin

This is the one I use most often. It accepts a day of week as an argument:

```
# ./checkin Tuesday


I look forward to hearing from you. I will follow up with you on **Tuesday** if I have not heard from you by then.


Thanks!

--
Brie
```


## newticket

I usually keep **2** files for each response that I write to a customer:

  - `notes.md` with loose, free-form notes, theories, ideas and links
  - `message.md`, `response.md`, `reply.md` with the response to the customer

Running `newticket` will `touch` files `notes.md` and `reply.md` in your current directory.
