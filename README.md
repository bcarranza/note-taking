# README

## Using new-ticket-notes.md

The frontmatter for this template is because I used [Zettlr](https://www.zettlr.com/) for managing notes based on this template.

## Using the scripts

Take a look at [bin/USAGE.md](bin/USAGE.md) for notes on using the super basic shell scripts in the `bin` directory. 

## Additional Tools 

I use `glow`, to [render the Markdown I have written on the CLI](https://github.com/charmbracelet/glow). 
