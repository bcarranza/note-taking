---
title: "TICKET TEMPLATE: XYZXYZ: PROBLEM DESCRIPTION"
keywords:
  - GitLab
  - Template
  - Tickets
  - Federal? Regular?
  - Stage? Create/Verify/Plan
---


# Ticket Description

## Question



## Research
  - [ ] Is there a clear problem statement?
  - [ ] Do you need additional information?
  - [ ] Does this user's license definitely grant them access to this feature?
  - [ ] Are there other similar tickets in ZenDesk?
  - [ ] Have you looked through the issue tracker?
  - [ ] Have you looked through the customer's previous tickets?
  - [ ] Would it make sense to inquire as to **why** the customer
### Reproducing the Error


### Summary of Findings
This section includes a summary of what you found during research and reproduction. Ideas worth pursuing go here. Other ideas stay up there. 

## Response

